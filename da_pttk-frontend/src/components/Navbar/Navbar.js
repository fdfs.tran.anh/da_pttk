import React from "react";
import styled from "styled-components";
import { NavbarData } from "./NavbarData";
import { IconContext } from "react-icons/lib";
import { Link } from 'react-router-dom'

export const Nav = styled.nav`
background: #15171c;
display: flex;
justify-content: space-between;
align-items: center;
width: 100%;
`;

export const NavLink = styled(Link)`
color: white;
padding: 0 20px;
font-size: 18px;
display: flex;
align-items: center;
text-decoration: none;
height: 100%;
cursor: pointer;
margin: auto;
&:hover {
	background-color: #04AA6D;
}
`;

export const NavMenu = styled.div`
display: flex;
align-items: center;
width: 100%;
z-index: 15;
height: 10vh;
`;

export const NavbarLabel = styled.span`
margin-left: 16px;
`;

const Navbar = () => {
	return (
		<>
		<IconContext.Provider value={{ color: "white" }}>
		<Nav>
		    <NavMenu>
				{NavbarData.map((item, index) => {
			        return (
						<NavLink to={item.path} key={index}>
							<div>
								{item.icon}
								<NavbarLabel>{item.title}</NavbarLabel>
							</div>
						</NavLink>
					)
				})}
		    </NavMenu>
		</Nav>
		</IconContext.Provider>
	</>
	);
};

export default Navbar;