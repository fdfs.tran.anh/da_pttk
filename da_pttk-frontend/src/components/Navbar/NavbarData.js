import React from "react";
import * as FaIcons from "react-icons/fa";

export const NavbarData = [
	{
		title: "Giám Sát",
		path: "/",
		icon: <FaIcons.FaMapMarkedAlt />,
	},
	{
		title: "Điểm Giao Dịch",
		path: "/diemgiaodich",
		icon: <FaIcons.FaMapMarker />,
	},
	
	{
		title: "Tuyến Mẫu",
		path: "/tuyenmau",
		icon: <FaIcons.FaRoute />,
	},
	// {
	// 	title: "Tìm Đường",
	// 	path: "/timduong",
	// 	icon: <FaIcons.FaReact />,
	// },
	{
		title: "Lịch Sử",
		path: "/lichsu",
		icon: <FaIcons.FaHistory />,
	},
	{
		title: "Test",
		path: "/test",
		icon: <FaIcons.FaReact />,
	},
];

export default NavbarData;