import React from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import GiamSat from './pages/giamsat';
import DiemGiaoDich from './pages/diemgiaodich';
import TuyenMau from './pages/tuyenmau';
import LichSu from './pages/lichsu';
import Test from './pages/test';

function App() {
	return (
		<Router>
			<Navbar/>
			  	<Switch>
				  	<Route path='/' exact component={GiamSat} />
			  		<Route path='/lichsu' component={LichSu} />
				  	<Route path='/diemgiaodich' component={DiemGiaoDich} />
				  	<Route path='/tuyenmau' component={TuyenMau} />
					<Route path='/test' component={Test} />
			  	</Switch>
		</Router>
	);
}

export default App;