import React from "react";
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import styled from "styled-components";
import {Button, Form, Table, Row, Col} from 'react-bootstrap';
import { useState } from "react";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;
  
const Side = styled.div`
  flex: 30%;
  padding: 20px;
  height: 90vh;
  overflow-y: scroll;
`;

const Main = styled.div`
  flex: 70%;
`;

const ContainerStyle = {
  width: '100%',
  height: '90vh'
};

const center = {
  lat: 21.005937,
  lng: 105.841893
};

const LichSu = () => {
  const [deviceId, setDeviceId] = useState("");
  const [receiveTime, setReceiveTime] = useState("");

  const HisApi = `http://localhost:5274/api/History/`;
  const HisIdApi = `http://localhost:5274/api/History/${deviceId}`;
  const HisIdTimeApi = `http://localhost:5274/api/History/${deviceId}/${receiveTime}`;

  const [HisData, setHisData] = useState([])

  const fetchDeviceData = () => {
    fetch(HisApi)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setHisData(data)
      })
  }

  const [HisIdData, setHisIdData] = useState([])

  const fetchTimeData = () => {
    fetch(HisIdApi)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setHisIdData(data)
      })
  }

  const [HisIdTimeData, setHisIdTimeData] = useState([])

  const fetchLatLngData = () => {
    fetch(HisIdTimeApi)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setHisIdTimeData(data)
      })
  }

  const handleChangeDevice = (event) => {
    setDeviceId(event.target.value)
  }

  const handleChangeTime = (event) => {
    setReceiveTime(event.target.value)
  }
    return (
      <Container>
        <Side>
          <Form>
            <h1>Thông tin</h1>
            <hr></hr>
            <Row className="mb-2">
              <Form.Group as={Col} className="mb-2">
                <Button variant="info" onClick={fetchDeviceData}>Device Call</Button>
              </Form.Group>
              <Form.Group as={Col} className="mb-2">
                <Form.Label>Có {HisData.length} thiết bị</Form.Label>
              </Form.Group>
            </Row>
            <Form.Group className="mb-2">
              <Form.Label>Chọn thiết bị:</Form.Label>
              <Form.Select value={deviceId} onChange={handleChangeDevice}>
              {HisData && HisData.map(({DeviceId}) => {
                return (
                  <option key={DeviceId} value={DeviceId}>{DeviceId}</option>
                );
              })}
              </Form.Select>
            </Form.Group>
            
            <hr></hr>
            <Row className="mb-2">
            <Form.Group as={Col} className="mb-2">
              <Button variant="info" onClick={fetchTimeData}>Time Call</Button>
            </Form.Group>
            <Form.Group as={Col} className="mb-2">
              <Form.Label>Có {HisIdData.length} ngày hoạt động</Form.Label>
            </Form.Group>
            </Row>
            <Form.Group className="mb-2">
              <Form.Label>Ngày hoạt động:</Form.Label>
              <Form.Select value={receiveTime} onChange={handleChangeTime}>
              {HisIdData && HisIdData.map(({DateReceiveTime}) => {
                return (
                  <option key={DateReceiveTime} value={DateReceiveTime}>{DateReceiveTime}</option>
                );
              })}
              </Form.Select>
            </Form.Group>
            
            <hr></hr>
            <Row className="mb-2">
            <Form.Group as={Col} className="mb-2">
              <Button variant="info" onClick={fetchLatLngData}>Map Call</Button>
            </Form.Group>
            <Form.Group as={Col} className="mb-2">
              <Form.Label>Có {HisIdTimeData.length} điểm</Form.Label>
            </Form.Group>
            </Row>
            <Row>
            <Table striped bordered hover>
              <thead style={{background:"#04AA6D"}}><tr><th>Id</th><th>Thời gian</th><th>Tọa độ</th><th>Trạng thái</th></tr></thead>
              <tbody>
              {HisIdTimeData && HisIdTimeData.map(({Id, ReceiveTime, Latitude, Longitude, State}) => {
                return (
                  <tr key={Id}>
                    <td>{Id}</td>
                    <td>{ReceiveTime}</td>
                    <td>{Latitude}, {Longitude}</td>
                    <td>{State}</td>
                  </tr>
                );
              })}
              </tbody>
            </Table>
            </Row>
          </Form> 
        </Side>

        <Main>
        <LoadScript googleMapsApiKey="AIzaSyAqnELf33fbLY-1gPm6tQBr4CvX6eQhhcc">
        <GoogleMap
          mapContainerStyle={ContainerStyle}
          center={center}
          zoom={13}
        >
          {HisIdTimeData && HisIdTimeData.map(({Id, Latitude, Longitude}) => {
            return (
              <Marker key={Id} position={{lat:Latitude, lng: Longitude}}>
              </Marker>
            );
          })}
        </GoogleMap>
      </LoadScript>
        </Main>
      </Container>
    );
  }

export default LichSu;