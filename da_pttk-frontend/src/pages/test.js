import React, { useRef, useState } from "react";

import {
  GoogleMap,
  DirectionsService,
  DirectionsRenderer, 
  LoadScript
} from "@react-google-maps/api";

 function Test() {
  const [directions, setDirections] = useState(google.maps.DirectionsResult);
  const count = useRef(0);

  const directionsCallback = (
    result: google.maps.DirectionsResult,
    status: google.maps.DirectionsStatus
  ) => {
    if (status === "OK" && count.current === 0) {
      count.current++;
      console.count();
      setDirections(result);
    }
  };

  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <LoadScript googleMapsApiKey="AIzaSyDCd6df4sUqkRyi5R1fpBc8hQYiqOmiTQ8">
      <GoogleMap
        mapContainerClassName="w-full h-full"
        mapContainerStyle={{ height: "300px" }}
        zoom={7}
      >
        <DirectionsService
          options={{
            origin: new google.maps.LatLng(19.69526676768985, -98.98627429537335),
            destination: new google.maps.LatLng(19.741276384055464, -98.95515398611924
            ),
            travelMode: google.maps.TravelMode.DRIVING,
            waypoints: [
              {location: new google.maps.LatLng(19.69613375169117, -98.96556819554857)
              },
              {
                location: new google.maps.LatLng(19.731744649438866, -98.95091284962213)
              }
            ]
          }}
          callback={directionsCallback}
        />
        {directions && <DirectionsRenderer directions={directions} />}
      </GoogleMap>
      </LoadScript>
    </div>
  );
}

export default Test;