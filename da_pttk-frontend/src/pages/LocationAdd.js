import React,{Component} from 'react';
import {Button, Row, Col, Form} from 'react-bootstrap';

export class LocationAdd extends Component{
    constructor(props){
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        fetch('http://localhost:5274/api/Locations',{
            method:'POST',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                LocationId:event.target.LocationId.value,
                LocationName:event.target.LocationName.value,
                Latitude:event.target.Latitude.value,
                Longitude:event.target.Longitude.value,
                Description:event.target.Description.value,
                UnitId:1
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
        },
        (error)=>{
            alert('Failed');
        })
    }
    render(){
        return (
            <div className="container">
            <Form onSubmit={this.handleSubmit}>
                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="LocationId">
                        <Form.Label>Mã điểm giao dịch:</Form.Label>
                        <Form.Control type="text" name="LocationId" required placeholder="LocationId"/>
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="LocationName">
                        <Form.Label>Tên điểm giao dịch:</Form.Label>
                        <Form.Control type="text" name="LocationName" required placeholder="LocationName"/>
                    </Form.Group>
                </Row>
                                
                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="Latitude">
                        <Form.Label>Vĩ độ:</Form.Label>
                        <Form.Control type="text" name="Latitude" required placeholder="Latitude"/>
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3" controlId="Longitude">
                        <Form.Label>Kinh độ:</Form.Label>
                        <Form.Control type="text" name="Longitude" required placeholder="Longitude"/>
                    </Form.Group>
                </Row>                

                <Form.Group controlId="Description" className="mb-3">
                    <Form.Label>Mô tả:</Form.Label>
                    <Form.Control as="textarea"  name="Description" required placeholder="Description"/>
                </Form.Group>
                <Form.Group><Button variant="primary" className="mb-3" type="submit">Thêm điểm</Button></Form.Group>
            </Form>
        </div>
        )
    }
}