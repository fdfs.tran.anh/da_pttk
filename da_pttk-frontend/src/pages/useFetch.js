import { useState, useEffect } from "react";

const useFetch = (url) => {
    const [data, setData] = useState(null);
  
    useEffect(() => {
        let interval = setInterval(() => {
            fetch(url)
            .then((res) => res.json())
            .then((data) => setData(data));
        }, 5000);
  
        return () => {
            clearInterval(interval);
        };
  
    }, [url]);
  
    return [data];
};

export default useFetch;