import React, { Component } from "react";
import styled from "styled-components";
import {Table, Button, ButtonToolbar} from 'react-bootstrap';
import {LocationAdd} from './LocationAdd';
import {EditLocModal} from './LocationEdit';
import {LocationMap} from './LocationMap';

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;
  
const Side = styled.div`
  flex: 30%;
  padding: 20px;
  height: 90vh;
  overflow-y: scroll;
`;

const Main = styled.div`
  flex: 70%;
`;

const mapContainerStyle = {
	width: '100%',
	height: '100%'
};
  
const center = {
	lat: 21.005937,
	lng: 105.841893
};


class DiemGiaoDich extends Component {
	
	constructor(props){
		super(props);
		this.state={locs:[], editModalShow:false}
	}
	
	refreshList(){
		fetch('http://localhost:5274/api/Locations')
		.then(response=>response.json())
		.then(data=>{
		  this.setState({locs:data});
		});
	}
	
	componentDidMount(){
		this.refreshList();
	}
	
	componentDidUpdate(){
		this.refreshList();
	}
	
	deleteLocation(locid){
		if(window.confirm('Are you sure?')){
			fetch('http://localhost:5274/api/Locations'+locid,{
				method:'DELETE',
				header:{'Accept':'application/json', 'Content-Type':'application/json'}
		  	})
		}
	}


	render() {
		const {locs, locid, locname, lat, long, des, unit}=this.state;
    	let editModalClose=()=>this.setState({editModalShow:false});
		return (
		  <Row>
			<Side>
				<h2>Thêm Điểm Giao Dịch</h2>
				<LocationAdd/>
				<hr/>

				<h2>Danh Sách Điểm Giao Dịch</h2>
				<Table className="mt-4" striped bordered hover size="sm">
					<thead style={{background:"#04AA6D"}}>
						<tr>
							<th>LocationId</th>
							<th>LocationName</th>
							<th>Description</th>
							<th>Options</th>
						</tr>
					</thead>
								
					<tbody>{locs.map(location=>
						<tr key={location.LocationId}>
						<td>{location.LocationId}</td>
						<td>{location.LocationName}</td>
						<td>{location.Description}</td>
						<td>
							<ButtonToolbar>
							<Button className="mr-2" variant="info"
							onClick={()=>this.setState({editModalShow:true,
							locid:location.LocationId,
							locname:location.LocationName,
							lat: location.Latitude,
							long: location.Longitude,
							des:location.Description,
							unit:location.UnitId})}>
								Sửa
							</Button>

							<Button className="mr-2" variant="danger"
							onClick={()=>this.deleteLocation(location.LocationId)}>
								Xóa
							</Button>

							<EditLocModal show={this.state.editModalShow}
								onHide={editModalClose}
								locid={locid}
								locname={locname}
								lat={lat}
								long={long}
								des={des}
								unit={unit}/>
							</ButtonToolbar>
						</td>
						</tr>)}
					</tbody>
				</Table>
			</Side>
	
			<Main>
				<LocationMap />
			</Main>
	
		  </Row>
		);
	  }
};

export default DiemGiaoDich;