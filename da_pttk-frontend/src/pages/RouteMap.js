import React, { useState } from "react";
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import useFetch from "./useFetch";
import Table from 'react-bootstrap/Table';

const containerStyle = {
  width: '100%',
  height: '90vh'
};

const center = {
  lat: 21.005937,
  lng: 105.841893
};

export const RouteMap = () => {
  const [data] = useFetch("http://localhost:5274/api/Routes");
  const [activeMarker, setActiveMarker] = useState(-1);

  const handleActiveMarker = (marker) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  const handleInfoCloseClick = () => {
    setActiveMarker(-1);
  };

  return (
    <LoadScript googleMapsApiKey="AIzaSyDCd6df4sUqkRyi5R1fpBc8hQYiqOmiTQ8">
      <GoogleMap
        onClick={() => setActiveMarker(-1)}
        mapContainerStyle={containerStyle}
        center={center}
        zoom={13}
      >
        
      </GoogleMap>
    </LoadScript>
  )
}