import React,{Component} from 'react';
import {Button, Row, Col, Form} from 'react-bootstrap';

export class RouLocAdd extends Component{
    constructor(props){
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        fetch('http://localhost:5274/api/Route_Location',{
            method:'POST',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                RouteId:event.target.RouteId.value,
                RouteName:event.target.RouteName.value,
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
        },
        (error)=>{
            alert('Failed');
        })
    }
    render(){
        return (
            <div className="container">
            <Form onSubmit={this.handleSubmit}>
                <Form.Group className="mb-3" controlId="RouteId">
                    <Form.Label>Mã tuyến:</Form.Label>
                    <Form.Control type="text" name="RouteId" required placeholder="Route Id"/>
                </Form.Group>
                
                <Form.Group className="mb-3" controlId="RouteName">
                    <Form.Label>Tên tuyến:</Form.Label>
                    <Form.Control type="text" name="RouteName" required placeholder="Route Name"/>
                </Form.Group>              

                <Form.Group controlId="Description" className="mb-3">
                    <Form.Label>Mô tả:</Form.Label>
                    <Form.Control as="textarea"  name="Description" required placeholder="Description"/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="UnitId">
                    <Form.Label>Đơn vị quản lý:</Form.Label>
                    <Form.Control type="text" name="UnitId" required placeholder="Unit Id"/>
                </Form.Group>  

                <Form.Group><Button variant="primary" className="mb-3" type="submit">Tạo tuyến</Button></Form.Group>
            </Form>
        </div>
        )
    }
}