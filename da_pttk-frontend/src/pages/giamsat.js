import React, { useState } from "react";
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import useFetch from "./useFetch";
import Table from 'react-bootstrap/Table';

const containerStyle = {
  width: '100vw',
  height: '90vh'
};

const center = {
  lat: 21.005937,
  lng: 105.841893
};

const GiamSat = () => {
  const [data] = useFetch("http://localhost:5274/api/Online");
  const [activeMarker, setActiveMarker] = useState(-1);

  const handleActiveMarker = (marker) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  const handleInfoCloseClick = () => {
    setActiveMarker(-1);
  };

  return (
    <LoadScript googleMapsApiKey="AIzaSyDCd6df4sUqkRyi5R1fpBc8hQYiqOmiTQ8">
      <GoogleMap
        onClick={() => setActiveMarker(-1)}
        mapContainerStyle={containerStyle}
        center={center}
        zoom={13}
      >
        {data && data.map(({ Id, ReceiveTime, Latitude, Longitude, State, DeviceId, CreatedAt, UpdatedAt, UserName}) => {
          return (

              <Marker key={DeviceId} position={{lat:Latitude, lng: Longitude}} onClick={() => handleActiveMarker(DeviceId)}>
                {activeMarker === DeviceId ? (
                  <InfoWindow onCloseClick={handleInfoCloseClick}>
                    <Table striped bordered hover>
                      <thead style={{background:"#04AA6D"}}>
                      <tr><th><b>Thuộc tính</b></th><th><b>Giá trị</b></th></tr>
                      </thead>
                      
                      <tbody>
                      <tr><td><b>DeviceId</b></td><td>{DeviceId}</td></tr>
                      <tr><td><b>State</b></td><td>{State}</td></tr>
                      <tr><td><b>UserName</b></td><td>{UserName}</td></tr>
                      <tr><td><b>ReceiveTime</b></td><td>{ReceiveTime}</td></tr>
                      <tr><td><b>CreatedAt</b></td><td>{CreatedAt}</td></tr>
                      <tr><td><b>UpdatedAt</b></td><td>{UpdatedAt}</td></tr>
                      <tr><td><b>Id</b></td><td>{Id}</td></tr>
                      <tr><td><b>Latitude</b></td><td>{Latitude}</td></tr>
                      <tr><td><b>Longitude</b></td><td>{Longitude}</td></tr>
                      </tbody>
                    </Table>
                  </InfoWindow>
                ) : null}
              </Marker>
          );
        })}
      </GoogleMap>
    </LoadScript>
  )
}

export default GiamSat;