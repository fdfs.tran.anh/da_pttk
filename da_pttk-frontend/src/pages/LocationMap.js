import React, { useState } from "react";
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import useFetch from "./useFetch";
import Table from 'react-bootstrap/Table';

const containerStyle = {
  width: '100%',
  height: '90vh'
};

const center = {
  lat: 21.005937,
  lng: 105.841893
};

export const LocationMap = () => {
  const [data] = useFetch("http://localhost:5274/api/Locations");
  const [activeMarker, setActiveMarker] = useState(-1);

  const handleActiveMarker = (marker) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  const handleInfoCloseClick = () => {
    setActiveMarker(-1);
  };

  return (
    <LoadScript googleMapsApiKey="AIzaSyDCd6df4sUqkRyi5R1fpBc8hQYiqOmiTQ8">
      <GoogleMap
        onClick={() => setActiveMarker(-1)}
        mapContainerStyle={containerStyle}
        center={center}
        zoom={13}
      >
        {data && data.map(({Id, LocationId, LocationName, Latitude, Longitude, Description, UnitId}) => {
          return (

              <Marker key={LocationId} position={{lat:Latitude, lng: Longitude}} onClick={() => handleActiveMarker(LocationId)}>
                {activeMarker === LocationId ? (
                  <InfoWindow onCloseClick={handleInfoCloseClick}>
                    <Table striped bordered hover>
                      <thead style={{background:"#04AA6D"}}>
                      <tr><th><b>Thuộc tính</b></th><th><b>Giá trị</b></th></tr>
                      </thead>
                      
                      <tbody>
                      <tr><td><b>LocationId</b></td><td>{LocationId}</td></tr>
                      <tr><td><b>LocationName</b></td><td>{LocationName}</td></tr>
                      <tr><td><b>Description</b></td><td>{Description}</td></tr>
                      <tr><td><b>UnitId</b></td><td>{UnitId}</td></tr>
                      <tr><td><b>Latitude</b></td><td>{Latitude}</td></tr>
                      <tr><td><b>Longitude</b></td><td>{Longitude}</td></tr>
                      <tr><td><b>Id</b></td><td>{Id}</td></tr>
                      </tbody>
                    </Table>
                  </InfoWindow>
                ) : null}
              </Marker>
          );
        })}
      </GoogleMap>
    </LoadScript>
  )
}