import React,{Component} from 'react';
import {Modal,Button, Row, Col, Form} from 'react-bootstrap';

export class EditLocModal extends Component{
  constructor(props){
    super(props);
    this.handleSubmit=this.handleSubmit.bind(this);
  }

  handleSubmit(event){
    event.preventDefault();
    fetch('http://localhost:5274/api/Locations',{
      method:'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
      body:JSON.stringify({
        LocationId:event.target.LocationId.value,
        LocationName:event.target.LocationName.value,
        Latitude:event.target.Latitude.value,
        Longitude:event.target.Longitude.value,
        Description:event.target.Description.value,
        UnitId:1
      })
    })
    .then(res=>res.json())
    .then((result)=>{
      alert(result);
    },
    (error)=>{
      alert('Failed');
    })
  }
  
  render(){
    return (
      <div className="container">
        <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        >
          <Modal.Header clooseButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Sửa Điểm
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col sm={6}>
                <Form onSubmit={this.handleSubmit}>
                  <Form.Group controlId="LocationId">
                    <Form.Label>ID Điểm</Form.Label>
                    <Form.Control type="text" name="LocationId" required
                      disabled
                      defaultValue={this.props.locid} 
                      placeholder="LocationId"/>
                  </Form.Group>

                  <Form.Group controlId="LocationName">
                    <Form.Label>Tên Điểm</Form.Label>
                    <Form.Control type="text" name="LocationName" required 
                      defaultValue={this.props.locname}
                      placeholder="LocationName"/>
                  </Form.Group>

                  <Form.Group controlId="Latitude">
                    <Form.Label>Vĩ Độ</Form.Label>
                    <Form.Control type="text" name="Latitude" required 
                      defaultValue={this.props.lat}
                      placeholder="Latitude"/>
                  </Form.Group>

                  <Form.Group controlId="Longitude">
                    <Form.Label>Kinh Độ</Form.Label>
                    <Form.Control type="text" name="Longitude" required 
                      defaultValue={this.props.long}
                      placeholder="Longitude"/>
                  </Form.Group>

                  <Form.Group controlId="Description">
                    <Form.Label>Mô Tả</Form.Label>
                    <Form.Control type="text" name="Description" required 
                      defaultValue={this.props.des}
                      placeholder="Description"/>
                  </Form.Group>

                  <Form.Group controlId="UnitId">
                    <Form.Label>Đơn Vị</Form.Label>
                    <Form.Control type="text" name="UnitId" required 
                      disabled
                      defaultValue={this.props.unit}
                      placeholder="UnitId"/>
                  </Form.Group>

                  <Form.Group>
                    <Button variant="primary" type="submit">
                      Cập Nhật
                    </Button>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Modal.Body>
            
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>Đóng</Button>
          </Modal.Footer>

        </Modal>

      </div>
    )
  }
}