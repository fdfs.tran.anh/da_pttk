import React, { Component } from "react";
import styled from "styled-components";
import {Table, Button, ButtonToolbar} from 'react-bootstrap';
import { RouteAdd } from "./RouteAdd";
import { RouteEdit } from "./RouteEdit";
import { RouteMap } from "./RouteMap";
import { RouLocAdd } from "./RouLocAdd";
import { RouLocEdit } from "./RouLocEdit";

const Main = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;
  
const Side = styled.div`
  flex: 30%;
  padding: 20px;
  height: 90vh;
  overflow-y: scroll;
`;

const Map = styled.div`
  flex: 70%;
`;

class TuyenMau extends Component {
	
	constructor(props){
		super(props);
		this.state={rous:[], editModalShow:false}
	}
	
	refreshList(){
		fetch('http://localhost:5274/api/Routes')
		.then(response=>response.json())
		.then(data=>{
		  this.setState({rous:data});
		});
	}
	
	componentDidMount(){
		this.refreshList();
	}
	
	componentDidUpdate(){
		this.refreshList();
	}
	
	deleteRoute(rouid){
		if(window.confirm('Are you sure?')){
			fetch('http://localhost:5274/api/Routes'+rouid,{
				method:'DELETE',
				header:{'Accept':'application/json', 'Content-Type':'application/json'}
		  	})
		}
	}

	render() {
		const {rous, rouid, rouname, des, unit}=this.state;
    	let editModalClose=()=>this.setState({editModalShow:false});
		return (
		  <Main>
			<Side style={{overflow: "scroll"}}>
				<h2>Thêm Điểm Giao Dịch</h2>
				<RouteAdd/>
				<hr/>

				<h2>Danh Sách Điểm Giao Dịch</h2>
				<Table className="mt-4" striped bordered hover size="sm">
					<thead style={{background:"#04AA6D"}}>
						<tr>
							<th>RouteName</th>
							<th>Options</th>
						</tr>
					</thead>
								
					<tbody>{rous.map(route=>
						<tr key={route.RouteId}>
						<td>{route.RouteName}</td>
						<td>
							<ButtonToolbar>
							<Button className="mr-2" variant="info"
							onClick={()=>this.setState({editModalShow:true,
							rouid:route.RouteId,
							rouname:route.RouteName,
							des:route.Description,
							unit:route.UnitId})}>
								Xem
							</Button>

							<Button className="mr-2" variant="danger"
							onClick={()=>this.deleteRoute(route.RouteId)}>
								Xóa
							</Button>

							<RouteEdit 
                show={this.state.editModalShow}
								onHide={editModalClose}
								rouid={rouid}
								rouname={rouname}
								des={des}
								unit={unit}/>
							</ButtonToolbar>
						</td>
						</tr>)}
					</tbody>
				</Table>
			</Side>
	
			<Map>
				<RouteMap />
			</Map>
	
		  </Main>
		);
	  }
};

export default TuyenMau;