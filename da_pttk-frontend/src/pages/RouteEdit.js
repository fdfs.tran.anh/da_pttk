import React,{Component} from 'react';
import {Modal,Button, Row, Col, Form} from 'react-bootstrap';

export class RouteEdit extends Component{
  constructor(props){
    super(props);
    this.handleSubmit=this.handleSubmit.bind(this);
  }

  handleSubmit(event){
    event.preventDefault();
    fetch('http://localhost:5274/api/Routes',{
      method:'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
      body:JSON.stringify({
        RouteId:event.target.LocationId.value,
        RouteName:event.target.LocationName.value,
        Description:event.target.Description.value,
        UnitId:1
      })
    })
    .then(res=>res.json())
    .then((result)=>{
      alert(result);
    },
    (error)=>{
      alert('Failed');
    })
  }
  
  render(){
    return (
      <div className="container">
        <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        >
          <Modal.Header clooseButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Sửa Tuyến
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col sm={6}>
                <Form onSubmit={this.handleSubmit}>
                  <Form.Group controlId="RouteId">
                    <Form.Label>Mã tuyến:</Form.Label>
                    <Form.Control type="text" name="RouteId" required
                      disabled
                      defaultValue={this.props.rouid} 
                      placeholder="Route Id"/>
                  </Form.Group>

                  <Form.Group controlId="RouteName">
                    <Form.Label>Tên tuyến:</Form.Label>
                    <Form.Control type="text" name="RouteName" required 
                      defaultValue={this.props.rouname}
                      placeholder="Route Name"/>
                  </Form.Group>

                  <Form.Group controlId="Description">
                    <Form.Label>Mô tả:</Form.Label>
                    <Form.Control type="text" name="Description" required 
                      defaultValue={this.props.des}
                      placeholder="Description"/>
                  </Form.Group>

                  <Form.Group controlId="UnitId">
                    <Form.Label>Đơn vị:</Form.Label>
                    <Form.Control type="text" name="UnitId" required 
                      disabled
                      defaultValue={this.props.unit}
                      placeholder="UnitId"/>
                  </Form.Group>

                  <Form.Group>
                    <Button variant="primary" type="submit">
                      Cập nhật tuyến
                    </Button>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Modal.Body>
            
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>Đóng</Button>
          </Modal.Footer>

        </Modal>

      </div>
    )
  }
}