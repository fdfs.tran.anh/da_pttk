﻿namespace API_Tracking.Models
{
    public class Routes
    {
        public int Id { get; set; }
        public int RouteId { get; set; }
        public string? RouteName { get; set; }
        public string? Description { get; set; }
        public int UnitId { get; set; }
        public string? State { get; set; }
    }
}
