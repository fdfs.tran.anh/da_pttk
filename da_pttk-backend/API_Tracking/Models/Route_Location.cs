﻿namespace API_Tracking.Models
{
    public class Route_Location
    {
        public int Id { get; set; }
        public int RouteId { get; set; }
        public int LocationId { get; set; }
    }
}
