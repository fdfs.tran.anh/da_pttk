﻿namespace API_Tracking.Models
{
    public class Online
    {
        public int Id { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? ReceiveTime { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string? State { get; set; }
        public double? PinPercentage { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string? UserName { get; set; }
    }
}
