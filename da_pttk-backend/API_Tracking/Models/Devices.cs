﻿namespace API_Tracking.Models
{
    public class Devices
    {
        public int DeviceId { get; set; }
        public string? DeviceImei { get; set; }
        public string? DeviceName { get; set; }
        public int UnitId { get; set; }
    }
}
