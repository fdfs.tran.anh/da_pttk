﻿using API_Tracking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace API_Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public LocationsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = "Select * from Location";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Locations location)
        {
            string query = @"Insert into Location 
                    (LocationId, LocationName, Latitude, Longitude, Description, UnitId) 
                    values 
                    (
                    '" + location.LocationId + @"'
                    ,'" + location.LocationName + @"'
                    ,'" + location.Latitude + @"'
                    ,'" + location.Longitude + @"'
                    ,'" + location.Description + @"'
                    ,  '" + location.UnitId + @"'
                    ) 
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Thêm mới thành công!");
        }

        [HttpPut]
        public JsonResult Put(Locations location)
        {
            string query = @"
                    Update Location set 
                    LocationName = '" + location.LocationName + @"'
                    ,Latitude = '" + location.Latitude + @"'
                    ,Longitude = '" + location.Longitude + @"'
                    ,Description = '" + location.Description + @"'
                    ,UnitId = '" + location.UnitId + @"' 
                    where LocationId = " + location.LocationId + @"
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Cập nhật thành công!");
        }

        [HttpDelete("{locationId}")]
        public JsonResult Delete(int locationId)
        {
            string query = @"
                Delete from Location 
                where LocationId = " + locationId + @"
                ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Xóa bỏ thành công!");
        }
    }
}