﻿using API_Tracking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace API_Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Route_LocationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public Route_LocationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("{routeId}")]
        public JsonResult Get(int routeId)
        {
            string query = @"Select * from Route_Location inner join Location on Route_Location.LocationId = Location.LocationId where RouteId = '"+routeId+"'";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Route_Location rocloc)
        {
            string query = @"Insert into Route_Location 
                    (RouteId, LocationId) 
                    values 
                    (
                    '" + rocloc.RouteId + @"'
                    ,'" + rocloc.LocationId + @"'
                    ) 
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Thêm mới thành công!");
        }

        [HttpPut]
        public JsonResult Put(Route_Location rocloc)
        {
            string query = @"
                    Update Route_Location set 
                    LocationId = '" + rocloc.LocationId + @"'
                    where Id = " + rocloc.Id + @"
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Cập nhật thành công!");
        }

        [HttpDelete("{roclocId}")]
        public JsonResult Delete(int roclocId)
        {
            string query = @"
                Delete from Route_Location 
                where Id = " + roclocId + @"
                ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Xóa bỏ thành công!");
        }
    }
}