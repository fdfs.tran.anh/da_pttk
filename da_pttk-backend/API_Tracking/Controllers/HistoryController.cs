﻿using API_Tracking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace API_Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public HistoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = "Select distinct DeviceId from History where DeviceId is not null order by DeviceId";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("{deviceId}")]
        public JsonResult Get(int deviceId)
        {
            //string query = @"Select CONVERT(DATE, MIN(ReceiveTime)) MinReviceTime, CONVERT(DATE, MAX(ReceiveTime)) MaxReviceTime from History " + "where DeviceId = " + deviceId;
            string query = @"Select distinct CONVERT(DATE, ReceiveTime) DateReceiveTime from History " + "where DeviceId = " + deviceId + "order by DateReceiveTime";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }


        [HttpGet("{deviceId}/{dateReceiveTime}")]
        public JsonResult Get(int deviceId, DateTime dateReceiveTime)
        {
            string query = @"Select Id, ReceiveTime, Latitude, Longitude, State, UserName from History where DeviceId = '" + deviceId + "' and CONVERT(DATE, ReceiveTime) = '" + dateReceiveTime + "'";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(History history)
        {
            string query = @"Insert into History (DeviceId, ReceiveTime, Latitude, Longitude, State, CreatedAt, UpdatedAt, UserName) values ('" + history.DeviceId + "', '" + history.ReceiveTime + "', '" + history.Latitude + "', '" + history.Longitude + "', '" + history.State + "', '" + history.CreatedAt + "', '" + history.UpdatedAt + "', '" + history.UserName + "') ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;

            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Thêm mới thành công!");
        }
    }
}