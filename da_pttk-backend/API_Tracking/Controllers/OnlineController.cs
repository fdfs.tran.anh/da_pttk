﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;
using API_Tracking.Models;

namespace API_Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OnlineController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public OnlineController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = "Select Id, ReceiveTime, Latitude, Longitude, State, DeviceId, CreatedAt, UpdatedAt, UserName from dbo.Online";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Online online)
        {
            string query = @"Insert into dbo.Online (DeviceId, ReceiveTime, Latitude, Longitude, State, CreatedAt, UpdatedAt, UserName) values ('" + online.DeviceId + "', '" + online.ReceiveTime + "', '" + online.Latitude + "', '" + online.Longitude + "', '" + online.State + "', '" + online.CreatedAt + "', '" + online.UpdatedAt + "', '" + online.UserName + "') ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;

            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Thêm mới thành công!");
        }

        [HttpPut]
        public JsonResult Put(Online online)
        {
            string query = @"Update Online set ReceiveTime = '" + online.ReceiveTime + "', Latitude = '" + online.Latitude + "', Longitude = '" + online.Longitude + "', State = '" + online.State + "', CreatedAt = '" + online.CreatedAt + "', UpdatedAt = '" + online.UpdatedAt + "', UserName = '" + online.UserName + "' " + "where DeviceId = " + online.DeviceId;
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Cập nhật thành công!");
        }

        [HttpDelete("{onlineId}")]
        public JsonResult Delete(int onlineId)
        {
            string query = @"
                Delete from dbo.Online 
                where DeviceId = " + onlineId + @"
                ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Xóa bỏ thành công!");
        }
    }
}