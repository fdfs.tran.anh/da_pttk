﻿using API_Tracking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace API_Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public RoutesController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = "Select * from Route";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Routes routes)
        {
            string query = @"Insert into Route 
                    (RouteId, RouteName, Description, UnitId, State) 
                    values 
                    (
                    '" + routes.RouteId + @"'
                    ,'" + routes.RouteName + @"'
                    ,'" + routes.Description + @"'
                    ,  '" + routes.UnitId + @"'
                    ,  '" + routes.State + @"'
                    ) 
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Thêm mới thành công!");
        }

        [HttpPut]
        public JsonResult Put(Routes routes)
        {
            string query = @"
                    Update Route set 
                    RouteName = '" + routes.RouteName + @"'
                    ,Description = '" + routes.Description + @"'
                    ,UnitId = '" + routes.UnitId + @"' 
                    ,State = '" + routes.State + @"' 
                    where RouteId = " + routes.RouteId + @"
                    ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Cập nhật thành công!");
        }

        [HttpDelete("{routesId}")]
        public JsonResult Delete(int routesId)
        {
            string query = @"
                Delete from Route 
                where RouteId = " + routesId + @"
                ";
            DataTable table = new DataTable();
            string sqlDataSourece = _configuration.GetConnectionString("BKeID");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSourece))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Xóa bỏ thành công!");
        }
    }
}